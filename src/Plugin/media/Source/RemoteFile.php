<?php

namespace Drupal\rocketship_download_content_block\Plugin\media\Source;

use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\Plugin\media\Source\File;

/**
 * File entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "remote_file",
 *   label = @Translation("Remote File"),
 *   description = @Translation("Use remote files for reusable media."),
 *   allowed_field_types = {"file"},
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class RemoteFile extends File {

}
